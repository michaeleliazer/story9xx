from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class MyUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1']

    error_messages = {
        'required' : 'Please Type',
        'unique': 'About user with this username already exists.',
        'password_mismatch' : "Password and confirm password don't match",
    }

    username_attrs = {
        'placeholder' : 'Username',
        'class' : 'form-control',
        'required' : True,
        'unique' : True,
    }

    password_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
    }

    username = forms.SlugField(label="Username",widget=forms.TextInput(attrs=username_attrs))
    password1 = forms.CharField(label="Password",widget=forms.PasswordInput(attrs=password_attrs))
    password2 = forms.CharField(label="Password",widget=forms.PasswordInput(attrs=password_attrs))
    
