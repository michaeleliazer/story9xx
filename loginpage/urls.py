from django.contrib import admin
from django.urls import path

from .views import loginView, registerView

app_name = 'loginpage'

urlpatterns = [
    path('login/', loginView, name="loginView"),
    path('register/', registerView, name="registerView"),
    # path('logout/', logoutUser, name="logoutUser"),
]