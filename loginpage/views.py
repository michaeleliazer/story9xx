from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import MyUserCreationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login , logout
from .models import Profile
from django.contrib.auth.models import User

# Create your views here.

def loginView(request):
    context = {
        'page_title':'LOGIN',
    }
    user = None

    if request.method == "GET":
        if request.user.is_authenticated:
            # logika untuk user
            return redirect("/")
        else:
            # logika untuk anonymous
            return render(request, 'login.html', context)


    if request.method == "POST":
        
        username_login = request.POST['username']
        password_login = request.POST['password']
        
        user = authenticate(request, username=username_login, password=password_login)

        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return redirect("/user/login/")

    return render(request, 'login.html', context)


response={}
def registerView(request):
    form = MyUserCreationForm(request.POST)
    if request.method=='POST':
        print(form.errors)
        if form.is_valid():
            userz = form.save()
            name = form.cleaned_data.get("username")
            passw = form.cleaned_data.get("password1")
            user = authenticate(username=name, password=passw)
            instance = Profile(user=userz)
            instance.save()
            messages.success(request, "User with name " + name +" is built successfully")
            return redirect ("/user/register/")
        else:
            messages.warning(request, form.errors)
    form = MyUserCreationForm()
    response['form'] = form
    return render(request, 'register.html', response)


@login_required
def logoutUser(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            logout(request)

            return redirect("/")

